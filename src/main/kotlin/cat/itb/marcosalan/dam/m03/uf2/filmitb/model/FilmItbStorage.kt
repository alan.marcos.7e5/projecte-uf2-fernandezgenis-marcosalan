package cat.itb.marcosalan.dam.m03.uf2.filmitb.model

import kotlinx.serialization.json.Json
import java.nio.file.Path
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlin.io.path.exists
import kotlin.io.path.readText
import kotlin.io.path.writeText

data class FilmItbStorage(val filePath: Path){

    //guarda datos de filmITB en un fichero
    fun save(dataFilmItb: FilmItb){
        val data = Json.encodeToString(dataFilmItb)
        filePath.writeText(data)
    }

    //leer los datos desde un fichero
    fun load(): FilmItb {
        return if (filePath.exists()) loadFromFile() else FilmItb()
    }

    private fun loadFromFile(): FilmItb {
        val data = filePath.readText()
        val serializer = Json { ignoreUnknownKeys = true }
        return serializer.decodeFromString<FilmItb>(data)
    }
}
