package cat.itb.marcosalan.dam.m03.uf2.filmitb.ui

import cat.itb.marcosalan.dam.m03.uf2.filmitb.model.FilmItb
import cat.itb.marcosalan.dam.m03.uf2.filmitb.model.User
import java.util.*


class UserUI(val scanner: Scanner, val appState: AppState){
    /**
     * shows the user section main menu
     */
    fun startUser(){
        println(toOutput())
        var action = scanner.nextLine().toInt()
        while (action != 0) {
            when (action){
                1-> addUser()
                2-> showMyUser()
                3-> viewUsers()
                4->updateUser()
                5->deleteUser()
                6->changeUser()
                7->showStatistics()
                else -> toOutputError("Invalid option")
            }
            println(toOutput())
            action = scanner.nextLine().toInt()
        }
    }

    private fun toOutput() : String{
        return  "Users:\n" +
                "1: Add user\n" +
                "2: Show my user\n" +
                "3: View users\n" +
                "4: Update user\n" +
                "5: Delete user\n" +
                "6: Change User\n" +
                "7: Show statistics\n" +
                "0: Return to main menu\n"
    }
    fun addUser() {
        println("Add a user")
        val name = scanner.nextLine()
        val user = User(name)
        appState.filmItb.addUser(user) //agrega usuario al filmItb de la appState
        println("User added")
    }
    private fun showMyUser() {
        println(appState.currentUser?.name)
    }
    private fun viewUsers(){
        println(appState.filmItb.listUser)
    }

    /**
     * changes the user name of the current user
     */
    private fun updateUser(){
        println("Enter your current user name")
        val currentUsername = scanner.nextLine()
        println("Enter your new username")
        val newUsername = scanner.nextLine()
        val update = appState.filmItb.updateUser(currentUsername,newUsername)
        verifyCurrentUpdate(update,currentUsername,appState.currentUser!!.name) //currentUser Nunca será null, sino, puteará.
    }

    /**
     * checks if its necessary to change the current user
     */
    private fun verifyCurrentUpdate(update: String?, currentUser: String, currentAppStateUser: String) {
        if (update != null) {
            val sameOfCurrent = sameOfCurrent(currentUser,currentAppStateUser)
            if (sameOfCurrent) {
                val newUser = appState.filmItb.searchUser(update)
                appState.changeUser(newUser)
            }
        }
        else toOutputError("The user to change does not exist")
    }

    /**
     * delete an user. can't delete an user if its not loged in or in the user list
     */
    private fun deleteUser() {
        println("Enter the name of the user to delete")
        val name = scanner.nextLine()
        val sameOfCurrent = sameOfCurrent(name,appState.currentUser!!.name)
        if (sameOfCurrent) toOutputError("Forbidden to delete the logged user") //evita que se borre el user actual
        else{
            val delete = appState.filmItb.deleteUser(name)
            if (delete != null) println("Deleted user")
            else toOutputError("The user to delete does not exist")//sale error porque el user no existe
        }
    }

    /**
     * changes the current user to an another in the user list
     */
    fun changeUser(){
        println("Select username")
        val name = scanner.nextLine()
        val newUser = appState.filmItb.searchUser(name) //comprueba que el user ingresado exista
        if (newUser != null) appState.changeUser(newUser)
        else toOutputError("The user does not exist") //sale error porque el usuario ingresado no existe
    }
    /*
   comprueba si el usuario que se ingresó es el mismo al actualmente logueado
    */
    private fun sameOfCurrent(currentUserName: String, currentAppStateUserName: String): Boolean {
        return currentUserName==currentAppStateUserName
    }

    /**
     * shows general stats of an user activity:
     * movies seen,
     * favourites,
     * last first movie seen
     */
    private fun showStatistics(){
        println(appState.filmItb.listUser)
        println("Enter an user to see their statistics")
        val name = scanner.nextLine()
        val user = appState.filmItb.searchUser(name)
        if (user != null) {
            val watchedSize = user.listWatchedFilms.size
            println("Watched movies: $watchedSize")
            println("favourite movies ${user.listFavouritesFilms.size}")
            if (watchedSize != 0) {
                println("first movie watched")
                println(user.listWatchedFilms[0].title)
                println("last movie whatched")
                println(user.listWatchedFilms.last().title)
            }else println("There are not watched movies")
        }else toOutputError("User does not exist")
    }
}

