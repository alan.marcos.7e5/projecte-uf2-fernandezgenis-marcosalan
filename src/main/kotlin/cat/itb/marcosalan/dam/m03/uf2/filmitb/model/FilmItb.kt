package cat.itb.marcosalan.dam.m03.uf2.filmitb.model

/**
 * La clase film ITB contiene la lógica de funcionamiento
 * @param[listFilm] es una lista de films añadidas a la plataforma
 * @param[listUser] es una lista de usuarios añadidos a la plataforma
 */
@kotlinx.serialization.Serializable
data class FilmItb(val listFilm: MutableList<Film> = mutableListOf(),
                   val listUser: MutableList<User> = mutableListOf()){
    /*
      FILMS
     */
    fun addFilm(film: Film) {
        listFilm.add(film)
    }

    /**
     * delete the film by their title from every list where it appears
     * @return title of film if film has been deleted or null if not
     */
    fun deleteFilm(title: String, currentUser: User): String?{
        val film = searchFilm(title)
        if (film != null) {
            listFilm.remove(film)
            deleteFilmFavourites(title,currentUser)
            deleteFilmWatched(title,currentUser)
            return title
        }

        return null
    }

    fun deleteFilmWatched(title: String, currentUser: User){
        val film = searchFilm(title)
        if(film!=null)
            currentUser.listWatchedFilms.remove(film)

//
//        if (currentUser != null) {
//            var toDelete: Film? = null
//            for (film in currentUser.listWatchedFilms) {
//                if (film.title == title) toDelete = film
//            }
//            if (toDelete != null) currentUser.listWatchedFilms.remove(toDelete)
//        }
    }

    fun deleteFilmFavourites(title: String, currentUser: User) {
            var toDelete: Film? = null
            for (film in currentUser.listFavouritesFilms) {
                if (film.title == title) toDelete = film
            }
            if (toDelete != null) currentUser.listFavouritesFilms.remove(toDelete)

    }

    /**
     * add the film to the list of watched films for the user
     * @param[user] the user that want to see the films
     */
    fun watchFilms(title: String, user: User) {
        val isWatched = isInWatchedFilmsList(title,user)
        for (i in listFilm.indices){
            if ((listFilm[i].title == title) && !isWatched){
                user.listWatchedFilms.add(listFilm[i])
            }
        }
    }
    fun isInWatchedFilmsList(title: String, user: User): Boolean{
        for (film in user.listWatchedFilms) {
            if (film.title == title) return true
        }
        return false
    }

    /**
     * count the times that the film was added like favourite
     */
    fun showLikes(film: Film): Int {
        var counted = 0
        for (user in listUser) {
            for (e in user.listFavouritesFilms) {
                if (e == film) counted += 1
            }
        }
        return counted
    }

    fun addFilmFavourites(title: String, currentUser: User){
        for (i in listFilm.indices) {
            if (listFilm[i].title == title) {
                currentUser.listFavouritesFilms.add(listFilm[i])
            }
        }
    }

    /**
     * verify that the film is in list of favourites
     * @param[currentUser] is the user in which will make the search of the film
     * @return true if film is in the list
     */
    fun isInFavouriteFilmList(title: String, currentUser: User): Boolean{
        for (film in currentUser.listFavouritesFilms) {
            if (film.title == title) return true
        }
        return false
    }

    /**
     * search the film by a given title
     */
    fun searchFilm(title: String): Film?{
        for (film in listFilm) {
            if (film.title == title) return film
        }
        return null
    }

    /**
     * search the user by a given user name
     */
    fun searchUser(username: String?): User?{
        for (user in listUser) {
            if (user.name == username) return user
        }
        return null
    }

    /*
      USERS
     */
    fun addUser(user: User){
        listUser.add(user)
    }
    /**
     * delete the user by his name from the user list
     * @return the user if user has been deleted or null if not
     */
    fun deleteUser(name: String): User?{
        val user = searchUser(name)
        return if (user != null) {
            listUser.remove(user)
            user
        } else null
    }

    /**
     * update the user name
     * @return the new user name or null if is not in list of user
     */
    fun updateUser(currentUser: String, newUser: String): String?{
        val user = searchUser(currentUser)
        if (user != null) {
            user.name = newUser
            return user.name
        }
        return null
    }

    /**
     * search films by a user request
     * @param[keyWord] the word to use in the search
     * @param[searchBy] the type of search
     * @return list of films that meet the requirements
     */
    fun searchFilmsBy(keyWord: String, searchBy: Int): MutableList<Film> {
        val listOfSearch = mutableListOf<Film>()
        for (film in listFilm) {
            when(searchBy){
                1->if (film.title == keyWord) listOfSearch += film
                2->if (film.director == keyWord) listOfSearch += film
                3->if (film.actor == keyWord) listOfSearch += film
                4->if (film.genre == keyWord) listOfSearch += film
                5->if (film.length == keyWord.toInt()) listOfSearch += film
            }
        }
        return listOfSearch
    }

    fun searchNotWatched(user: User): MutableList<Film> {
        val listOfNotWatched = mutableListOf<Film>()
        for (e in listFilm) {
            if (e !in user.listWatchedFilms) listOfNotWatched += e
        }
        return listOfNotWatched
    }

/*    fun showRecomended(user: User): Film? {
        val listOfRecommended = mutableListOf<Film>()
        listOfRecommended += mostLiked()
        listOfRecommended +
    }*/

    fun mostLiked(): Film? {
        var maxLikes = 0
        var maxFilm: Film? = null
        for (film in listFilm) {
            val likes = showLikes(film)
            if (maxLikes < likes) {
                maxLikes = likes
                maxFilm = film
            }
        }
        return maxFilm
    }
    fun mostWatched(): Film? {
        var maxWatch = 0
        var maxFilm: Film? = null
        for (film in listFilm) {
            val watches = showWatches(film)
            if (maxWatch < watches) {
                maxWatch = watches
                maxFilm = film
            }
        }
        return maxFilm
    }
    fun showWatches(film: Film): Int {
        var counted = 0
        for (user in listUser) {
            for (e in user.listWatchedFilms) {
                if (e == film) counted += 1
            }
        }
        return counted
    }

    /**
     * search the watched genres of a given user
     */
    fun searchGenres(user: User): MutableList<String> {
        val listOfGen = mutableListOf<String>()
        for (film in user.listWatchedFilms) {
            listOfGen += film.genre
        }
        return listOfGen
    }

    /**
     * search the most common genre of a list of genres
     * @return the most watched genre or null if not
     */
    fun searchFavouriteGenre(list: MutableList<String>): String?{
        val hashCount = list.groupingBy { it }.eachCount() //cuenta entradas de cada valor
        val maxValue = hashCount.values.maxOrNull() //obtiene el maximo valor
        for (k in hashCount) {
            if (k.value == maxValue) return k.key //retorna key del maximo valor (género por el cual buscar)
        }
        return null

    }

    /**
     * search by most common genre
     * @see searchFilmsBy
     */
    fun filmsByFavouriteGenre(user: User): List<Film> {
        val listOfGenres = searchGenres(user)
        val favouritesGenres = searchFavouriteGenre(listOfGenres)
        return searchFilmsBy(favouritesGenres!!, 4)
    }
}
