package cat.itb.marcosalan.dam.m03.uf2.filmitb.ui

import java.util.*

class SearchUI(val scanner: Scanner, val appState: AppState){
    /**
     * shows the search section main menu
     */
    fun startSearch() {
        println(toOutput())
        var action = scanner.nextLine().toInt()
        while (action != 0){
            when (action){
                1->searchByTitle(action)
                2->searchByDirector(action)
                3->searchByMainActor(action)
                4->searchByGenere(action)
                5->searchByLength(action)
                6->searchByNotWatched()
                7->searchByRecomended()
                else -> toOutputError("Invalid option")
            }
            println(toOutput())
            action = scanner.nextLine().toInt()
        }
    }
    private fun toOutput() : String{
        return "Search methods:\n" +
                "1: By title\n" +
                "2: By director\n" +
                "3: By main actor\n" +
                "4: By genere\n" +
                "5: By length\n" +
                "6: Not watched\n" +
                "7: Recomended\n" +
                "0: Return to main menu"
    }

    //Las recomendaciones son en base a los me gusta Y las vistas de todos los usuarios
    private fun searchByRecomended() {
        val user = appState.currentUser!!
        //by likes
        val recomendedPerLikes = appState.filmItb.mostLiked()
        if (recomendedPerLikes != null){
            println("Recomended film per LIKES from anothers users:\n$recomendedPerLikes")
        }else println("No films in list of favourites for any user")
        //by watchs
        val recomendedPerView = appState.filmItb.mostWatched()
        if (recomendedPerView != null){
            println("Recomended film per WATCHES from anothers users:\n$recomendedPerView")
        }else println("No films watched for any user")
        //by genre
        if (user.listWatchedFilms.isNotEmpty()) {
            val perGenre = appState.filmItb.filmsByFavouriteGenre(user)
            println("Recomended film per GENRE:\n$perGenre")
        }else searchByNotWatched()
    }

    private fun searchByNotWatched() {
        val user = appState.currentUser!!
        val listOfNotWatched = appState.filmItb.searchNotWatched(user)
        println("Not watched: $listOfNotWatched")
    }

    private fun searchByLength(action: Int) {
        println("Enter length")
        val length= scanner.nextLine()
        val listOfSearch = appState.filmItb.searchFilmsBy(length,action)
        if (listOfSearch.isNotEmpty()) println(listOfSearch)
        else println("There are not films with this length")
    }

    private fun searchByGenere(action: Int) {
        println("Enter genre")
        val genre = scanner.nextLine()
        val listOfSearch = appState.filmItb.searchFilmsBy(genre,action)
        if (listOfSearch.isNotEmpty()) println(listOfSearch)
        else println("There are not films for this genre")
    }

    private fun searchByMainActor(action: Int) {
        println("Enter main actor")
        val actor = scanner.nextLine()
        val listOfSearch = appState.filmItb.searchFilmsBy(actor,action)
        if (listOfSearch.isNotEmpty()) println(listOfSearch)
        else println("Actor without films")
    }

    private fun searchByDirector(action: Int) {
        println("Enter director")
        val director = scanner.nextLine()
        val listOfSearch = appState.filmItb.searchFilmsBy(director,action)
        if (listOfSearch.isNotEmpty()) println(listOfSearch)
        else println("Director without films")
    }

    fun searchByTitle(action: Int) {
        println("Enter a title")
        val title = scanner.nextLine()
        val listOfSearch = appState.filmItb.searchFilmsBy(title,action)
        if (listOfSearch.isNotEmpty()) println(listOfSearch)
        else println("There are not films with this title")
    }
}