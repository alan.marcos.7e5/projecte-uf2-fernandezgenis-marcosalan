package cat.itb.marcosalan.dam.m03.uf2.filmitb.ui

import cat.itb.marcosalan.dam.m03.uf2.filmitb.model.FilmItb
import cat.itb.marcosalan.dam.m03.uf2.filmitb.model.FilmItbStorage
import java.nio.file.Path
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists

/**
 * It contains the main where the only scanner of the project is declared.
 * The main method will only do one action, call the start () method of the UI class.
 * The start method must make the necessary initializations, display the menu and read the chosen option.
 */

data class UI (val scanner: Scanner = Scanner(System.`in`)){
    val filePath = createPath()
    val filmItbStorage = FilmItbStorage(filePath)
    val filmItb = filmItbStorage.load()
    val appState = AppState(null,filmItb)
    val userUI = UserUI(scanner, appState)
    val filmUI = FilmUI(scanner,appState)
    val searchUI = SearchUI(scanner,appState)

    fun start(){
        if (filmItb.listUser.isEmpty()) userUI.addUser()
        while(appState.currentUser==null)
            userUI.changeUser()
        println(toOutput())
        var action = scanner.nextLine().toInt()
        while (action != 0) {
            when (action){
                1-> userUI.startUser()
                2-> filmUI.startFilm()
                3-> searchUI.startSearch()
                else ->println(toOutputError("Invalid option"))
            }
            println(toOutput())
            action = scanner.nextLine().toInt()
        }
        filmItbStorage.save(filmItb)
    }
    private fun toOutput(): String {
        return "Welcome to FilmItb:\n" +
                "1: User\n" +
                "2: Films\n" +
                "3: Search\n" +
                "0: Exit\n"
    }

}

fun main() {
    UI().start()
}
fun toOutputError(errorType: String="") {
    println("An error occurred: $errorType")
}

fun createPath(): Path{
    val homePath = Path(System.getProperty("user.home"))
    val filePath = homePath.resolve("FilmItb.json")
    return filePath
}

