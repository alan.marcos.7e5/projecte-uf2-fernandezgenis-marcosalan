package cat.itb.marcosalan.dam.m03.uf2.filmitb.ui

import cat.itb.marcosalan.dam.m03.uf2.filmitb.model.FilmItb
import cat.itb.marcosalan.dam.m03.uf2.filmitb.model.User

/**
 * keeps all the program's information to avoid being deleted on the run
 */
data class AppState (var currentUser: User?, val filmItb: FilmItb) {
    fun changeUser(user: User?) {
        this.currentUser = user
    }
}


