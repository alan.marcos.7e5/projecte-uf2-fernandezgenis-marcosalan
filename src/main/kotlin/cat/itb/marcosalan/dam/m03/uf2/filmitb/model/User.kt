package cat.itb.marcosalan.dam.m03.uf2.filmitb.model
@kotlinx.serialization.Serializable
data class User(var name: String, val listFavouritesFilms: MutableList<Film> = mutableListOf(),
                val listWatchedFilms: MutableList<Film> = mutableListOf())