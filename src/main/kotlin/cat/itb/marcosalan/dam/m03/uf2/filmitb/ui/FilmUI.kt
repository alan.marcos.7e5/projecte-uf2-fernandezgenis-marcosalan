package cat.itb.marcosalan.dam.m03.uf2.filmitb.ui

import cat.itb.marcosalan.dam.m03.uf2.filmitb.model.Film
import java.util.*

class FilmUI(val scanner: Scanner, val appState: AppState){
    /**
     * shows the film section main menu
     */
    fun startFilm(){
        println(toOutput())
        var action = scanner.nextLine().toInt()
        while (action != 0){
            when(action){
                1->addFilm()
                2->showFilms()
                3->deleteFilm()
                4->watchFilms()
                5->viewWatchedFilms()
                6->addFilmFavourites()
                7->showFavourites()
                8->showLikesFilm()
                else -> toOutputError("Invalid option")
            }
            println(toOutput())
            action = scanner.nextLine().toInt()
        }
    }

    private fun toOutput() : String{
        return "Films:\n" +
                "1: Add film\n" +
                "2: Show films\n" +
                "3: Delete films\n" +
                "4: Watch films\n" +
                "5: View watched films\n" +
                "6: Add film to favorites\n" +
                "7: Show favorites\n" +
                "8: Show likes per film\n" +
                "0: Return to main menu\n"
    }
    private fun showLikesFilm() {
        println("choose a film")
        val title = scanner.nextLine()
        val film = appState.filmItb.searchFilm(title)
        if (film != null) println(appState.filmItb.showLikes(film))
        else toOutputError("The film has not been added")
    }
    private fun showFavourites() {
        val user = appState.currentUser!!
        println(user.listFavouritesFilms)
    }
    private fun addFilmFavourites() {
        println("Add film to favourites")
        val title = scanner.nextLine()
        val currentUser = appState.currentUser!!
        val isInFavourites = appState.filmItb.isInFavouriteFilmList(title,currentUser)
        val isInListFilms = appState.filmItb.searchFilm(title)
        if (isInFavourites) println("Film is already like yours favourites")
        else if (isInListFilms == null) println(toOutputError("The film has not been added"))
        else appState.filmItb.addFilmFavourites(title,currentUser) //agrega a favoritos si no fue agregada anteriormente
    }

    /**
     * shows a list of the current user watched movies
     */
    private fun viewWatchedFilms() {
        val user = appState.currentUser!!
        println(user.listWatchedFilms)
    }
    private fun watchFilms() {
        println("Select the film to watch")
        val title = scanner.nextLine()
        val user = appState.currentUser!!
        appState.filmItb.watchFilms(title,user)
        println("Now, the film is watched")
    }
    private fun deleteFilm() {
        println("Enter the name of the film to delete")
        val title = scanner.nextLine()
        val currentUser = appState.currentUser!!
        val delete = appState.filmItb.deleteFilm(title,currentUser)//borra la peli de la instancia appState o retorna null si no pudo
        if (delete==null) println(toOutputError("The film does not exist"))
        else println("Deleted film")
    }
    private fun showFilms() {
        println(appState.filmItb.listFilm)
    }

    /**
     * adds a film to the general program
     */
    private fun addFilm() {
        println("Add film data\nTitle")
        val title = scanner.nextLine()
        println("Director")
        val director = scanner.nextLine()
        println("Actor")
        val actor = scanner.nextLine()
        println("Genre")
        val genre = scanner.nextLine()
        println("Length")
        val length = scanner.nextInt()
        scanner.nextLine()
        val film = Film(title,director,actor,genre,length)
        appState.filmItb.addFilm(film) //agrega la peli de la instancia appState
        println("Added film")
    }
}
