package cat.itb.marcosalan.dam.m03.uf2.filmitb.model

import kotlinx.serialization.Serializable

@Serializable
data class Film(val title: String, val director: String, val actor: String,
                val genre: String, val length: Int)