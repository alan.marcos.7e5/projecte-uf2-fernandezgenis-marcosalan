package cat.itb.marcosalan.dam.m03.uf2.filmitb.model

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class FilmItbTest {
    val film = Film("Tit","Sp","LC","amor",1)
    val film2 = Film("TIT","Sp","LC","amor",2)
    val film3 = Film("OTRA","Sp","LC","accion",3)
    val user = User("alan")
    val user2 = User("genis")

    //TESTS DE FILMS
    @Test
    fun addFilmTest() {
        val filmItb = FilmItb()
        filmItb.addFilm(film)
        val result = filmItb.listFilm
        assertEquals(listOf<Film>(film),result)
    }
    @Test
    fun deleteFilmTestExists() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        val result = filmItb.deleteFilm("Tit",user)
        assertEquals("Tit",result)
    }
    @Test
    fun deleteFilmTestNotExists() {
        val filmItb = FilmItb()
        filmItb.addFilm(film)
        val result = filmItb.deleteFilm("Titanic",user)
        assertEquals(null,result)
    }
    @Test
    fun deleteFilmFavouritesNotNullTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilmFavourites("Tit",user)
        filmItb.addFilmFavourites("TIT",user)
        filmItb.deleteFilmFavourites("Tit",user)
        val result = user.listFavouritesFilms
        assertEquals(listOf<Film>(film2),result)
    }
    @Test
    fun deleteFilmWatchedTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.watchFilms("Tit",user)
        filmItb.watchFilms("TIT",user)
        filmItb.deleteFilmWatched("Tit",user)
        val result = user.listWatchedFilms
        assertEquals(listOf<Film>(film2),result)
    }

    @Test
    fun deleteFilmFavouritesNullTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilmFavourites("Tit",user)
        filmItb.addFilmFavourites("TIT",user)
        filmItb.deleteFilmFavourites("PELI INEXISTENTE",user)
        val result = user.listFavouritesFilms
        assertEquals(listOf<Film>(film,film2),result)
    }

    @Test
    fun addFilmFavouritesTest(){
        val filmItb = FilmItb()
        val user = User("alan", mutableListOf<Film>(film))
        filmItb.addFilmFavourites("Tit",user)
        val result = user.listFavouritesFilms
        assertEquals(listOf(film),result)
    }
    @Test
    fun isInFavouriteFilmListTest(){
        val filmItb = FilmItb()
        val user = User("alan", mutableListOf<Film>(film))
        filmItb.addUser(user)
        filmItb.addFilmFavourites("Tit",user)
        val result = filmItb.isInFavouriteFilmList("Tit",user)
        assertTrue(result)
    }
    @Test
    fun isInFavouriteFilmListFalseTest(){
        val filmItb = FilmItb()
        val user = User("alan", mutableListOf<Film>(film))
        filmItb.addUser(user)
        filmItb.addFilmFavourites("Tit",user)
        val result = filmItb.isInFavouriteFilmList("TIT",user)
        assertFalse(result)
    }

    @Test
    fun watchFilmsTest(){
        val filmItb = FilmItb()
        val user = User("alan", mutableListOf<Film>(film))
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.watchFilms("Tit",user)
        val result = user.listWatchedFilms
        assertEquals(listOf(film),result)
    }
    @Test
    fun watchFilmsTestAlreadyWatched(){
        val filmItb = FilmItb()
        val user = User("alan", mutableListOf<Film>(film), mutableListOf(film))
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.watchFilms("Tit",user) //el usuario siempre será correcto
        val result = user.listWatchedFilms
        assertEquals(listOf(film),result) //compruebo que no la duplique en lista
    }
    @Test
    fun watchFilmsTestFilmNotExists(){
        val filmItb = FilmItb()
        val user = User("alan", mutableListOf<Film>(film))
        filmItb.addUser(user)
        filmItb.watchFilms("Tit",user) //el usuario siempre será correcto
        val result = user.listWatchedFilms
        assertEquals(listOf<Film>(),result) //no podrá agregarla porque no existe
    }

    @Test
    fun isInWatchedFilmsListTestTrue(){
        val filmItb = FilmItb()
        val user = User("alan", mutableListOf(), mutableListOf(film))
        filmItb.addUser(user)
        val result = filmItb.isInWatchedFilmsList("Tit",user)
        assertTrue(result)
    }
    @Test
    fun isInWatchedFilmsListTestFalse(){
        val filmItb = FilmItb()
        filmItb.addUser(user)
        val result = filmItb.isInWatchedFilmsList("Tit",user)
        assertFalse(result)
    }
    @Test
    fun searchFilmTest(){
        val filmItb = FilmItb()
        filmItb.addFilm(film)
        val result = filmItb.searchFilm("Tit")
        assertEquals(film,result)
    }
    @Test
    fun searchFilmTestNotExists(){
        val filmItb = FilmItb()
        val result = filmItb.searchFilm("Tit")
        assertEquals(null,result)
    }
    @Test
    fun searchUserTest(){
        val filmItb = FilmItb()
        filmItb.addUser(user)
        val result = filmItb.searchUser("alan")
        assertEquals(user,result)
    }
    @Test
    fun searchUserNotExistTest(){
        val filmItb = FilmItb()
        val result = filmItb.searchUser("alan")
        assertEquals(null,result)
    }
    @Test
    fun showLikesTestFilm(){
        val filmItb = FilmItb()
        val result = filmItb.showLikes(film)
        assertEquals(0,result)
    }

    //TESTS DE USERS
    @Test
    fun addUserTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        val resultList = listOf<User>(user)
        assertEquals(resultList,filmItb.listUser)
    }
    @Test
    fun deleteUserTestNotNull() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        val result = filmItb.deleteUser("alan")
        assertEquals(user,result)
    }
    @Test
    fun deleteUserTestNull() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        val result = filmItb.deleteUser("usuario que no existe")
        assertEquals(null,result)
    }
    @Test
    fun updateUserTestNotNull() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        val result = filmItb.updateUser("alan","NUEVO ALAN")
        assertEquals("NUEVO ALAN",result)
    }
    @Test
    fun updateUserTestNull() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        val result = filmItb.updateUser("usuario que no existe","NUEVO ALAN")
        assertEquals(null,result)
    }
    //TEST OF SEARCH
    @Test
    fun searchFilmsByTitleTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        val result = filmItb.searchFilmsBy("Tit",1)
        assertEquals(listOf(film),result)
    }
    @Test
    fun searchFilmsByDirTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        val result = filmItb.searchFilmsBy("Sp",2)
        assertEquals(listOf(film,film2),result)
    }
    @Test
    fun searchFilmsByActorTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        val result = filmItb.searchFilmsBy("LC",3)
        assertEquals(listOf(film,film2),result)
    }
    @Test
    fun searchFilmsByGenTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilm(film3)
        val result = filmItb.searchFilmsBy("amor",4)
        assertEquals(listOf(film,film2),result)
    }
    @Test
    fun searchFilmsByLenTest() {
        val filmItb = FilmItb()
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        val result = filmItb.searchFilmsBy("2",5)
        assertEquals(listOf(film2),result)
    }
    @Test
    fun searchFilmsByNotWatchedTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.watchFilms("Tit",user)
        val result = filmItb.searchNotWatched(user)
        assertEquals(listOf(film2),result)
    }
    @Test
    fun mostLikedTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addUser(user2)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilm(film3)
        filmItb.addFilmFavourites("Tit",user)
        filmItb.addFilmFavourites("Tit",user2)
        filmItb.addFilmFavourites("OTRA",user2)
        val result = filmItb.mostLiked()
        assertEquals(film,result)
    }
    @Test
    fun mostWatchedTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addUser(user2)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilm(film3)
        filmItb.watchFilms("Tit",user)
        filmItb.watchFilms("Tit",user2)
        filmItb.watchFilms("OTRA",user2)
        val result = filmItb.mostWatched()
        assertEquals(film,result)
    }
    @Test
    fun showWatchesTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addUser(user2)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilm(film3)
        filmItb.watchFilms("Tit",user)
        filmItb.watchFilms("Tit",user2)
        filmItb.watchFilms("OTRA",user2)
        val result = filmItb.showWatches(film)
        assertEquals(2,result)
    }
    @Test
    fun searchGenresTest() {
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilm(film3)
        filmItb.watchFilms("Tit",user)
        filmItb.watchFilms("TIT",user)
        filmItb.watchFilms("OTRA",user)
        val result = filmItb.searchGenres(user)
        val expected = listOf<String>("amor","amor","accion")
        assertEquals(expected,result)
    }
    @Test
    fun searchGenresWithoutWatchedTest() { //retorna lista vacía, porque user no vio ningún film
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addUser(user2)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilm(film3)
        val result = filmItb.searchGenres(user)
        assertEquals(listOf<String>(),result)
    }
    @Test
    fun searchFavouriteGenreTest() { //retorna genero favorito
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilm(film3)
        filmItb.watchFilms("Tit",user)
        filmItb.watchFilms("TIT",user)
        filmItb.watchFilms("OTRA",user)
        val lisOfFavourites = filmItb.searchGenres(user)
        val result = filmItb.searchFavouriteGenre(lisOfFavourites)
        assertEquals("amor",result)
    }
    @Test
    fun searchFavouriteGenreWithoutWatchedTest() { //retorna genero null porque no hay un genero favorito
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilm(film3)
        val lisOfFavourites = filmItb.searchGenres(user)
        val result = filmItb.searchFavouriteGenre(lisOfFavourites)
        assertEquals(null,result)
    }
    @Test
    fun filmsByFavouriteGenreTest() { //retorna lista de pelis con el genero favorito
        val filmItb = FilmItb()
        filmItb.addUser(user)
        filmItb.addFilm(film)
        filmItb.addFilm(film2)
        filmItb.addFilm(film3)
        filmItb.watchFilms("Tit",user)
        filmItb.watchFilms("TIT",user)
        filmItb.watchFilms("OTRA",user)
        val result = filmItb.filmsByFavouriteGenre(user)
        val expected = listOf<Film>(film,film2)
        assertEquals(expected,result)
    }
}